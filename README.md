# LandingPageCivitas

Repositório para a Landing Page do Civitas

1.	[Como usar](#como-usar)
2.	[Tecnologias](#tecnologias)
3.	[Funcionalidades](#funcionalidades)
4.	[O que aprendi](#o-que-aprendi)

### Como usar<br>

Para usar o projeto você precisa rodar o seguinte comando dentro da pasta raiz do projeto:
    
    npm install

O npm instalará as dependecias necessarias.
Após terminar você deve digitar outro comando na pasta raiz:

    npx json-server --watch db.json

E então é só abrir o arquivo `index.html` pelo [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer) e se divertir!

Para fechar o servidor basta pressionar CTRL+C no terminal.

(Atenção: o projeto é de uma iniciante, então não há (ainda) nenhum tipo de proteção aos dados, não coloque dados sensíveis no formulário ou então os apague antes de subir sua versão!)


### Tecnologias<br>

O site foi feito usando HTML5, CSS3 e JavaScript.

### Funcionalidades <br>

O site possui:
- aumento das imagens ao passar o mouse em cima
- carrossel animado que pausa ao descansar o mouse em cima
- crud completo

### O que aprendi

Fazendo esse site eu:

- aprendi a fazer um carrossel animado com JavaScript puro, usando setInterval()
- aprimorei meu aprendizado de flexbox
- melhorei minhas habilidades em HTML e CSS
- aprendi um pouco mais de Markdown para fazer esse ReadMe
- aprendi como implementar os verbos PUT, GET, UPDATE e DELETE.
- aprendi a usar json-server para simular uma API
- entendi como funcionam as requisições a APIs
