import deleteUser from "../controllers/deleteUser.js";

/**
 * Criar o botão de atualizar a User
 * - redireciona para a página atualizar.html
 * - passa como params o Id da User
 */
export const createUpdateBtn = (id) => {
	const { button } = createButton(id, "Editar", "update-btn");

	button.addEventListener("click", () => {
		window.location.href = `../../public/update.html?id=${id}`;
	});
};

/* criar o botão para deletar a User */
export const createDeleteBtn = (id) => {
	const { button } = createButton(id, "Excluir", "delete-btn");

	button.addEventListener("click", async (event) => {
		event.preventDefault();
		await deleteUser(id);
	});
};

/**
 * criar o botão de concluir a User
 *
 */
// export const createConcBtn = (id) => {
// 	const { button, tr } = createButton(id, "Concluir", "conclude-btn");

// 	button.addEventListener("click", (event) => {
// 		event.preventDefault();
// 		tr.classList.toggle("concluded");
// 		button.textContent == "Concluir"
// 			? (button.textContent = "Desmarcar")
// 			: (button.textContent = "Concluir");
// 	});
// };

/* criar o botão padrão */

const createButton = (id, btnText, btnClass) => {
	const newTr = document.querySelector(`.new-tr-${id}`);
	const btn = document.createElement("button");
	btn.textContent = btnText;
	btn.classList.add(btnClass);
	newTr.appendChild(btn);

	return {
		button: btn,
		tr: newTr,
	};
};
