// Script par o funcionamento do carrossel
const slides = document.getElementById("slides") //pega a div que estão os slides
const slide = document.querySelectorAll("#slides div") //pega todas as divs dentro do elemento com id #slides
const divCarrossel = document.querySelector(".carrossel");
divCarrossel.addEventListener("mouseover",pararCarrossel);
divCarrossel.addEventListener("mouseout",rodarCarrossel);

var idSlide = 0;
var aux = 1;

function carrossel(){ //função pra passar de slide
    idSlide++;
    if(idSlide>slide.length-1){
        idSlide=0;
    }

    slides.style.transform = `translatex(${-idSlide*600}px)`; //avança pro proximo slide ou volta pro primeiro se estiver no ultimo
}

var intervalo = 1500; 
var refreshIntervalId = setInterval(carrossel, intervalo); //determina o tempo slide

function rodarCarrossel(){
    if(aux){ //evita que acumule vários setInterval e fique chamando mais de uma vez a função pra passar de slide
        refreshIntervalId = setInterval(carrossel, intervalo);
    }
    aux=0;
}

function pararCarrossel(){ //quando descansar o mouse sobre o slide vai parar de passar
    aux=1;
    clearInterval(refreshIntervalId); //para o setInterval e para de rodar o slide
}
//Fim do script do carrossel