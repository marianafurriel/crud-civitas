const showList = (content) => {
  console.log(content);
  const th = document.querySelector(".headerTable");
  th.classList.toggle("invisible");
  return `
        <tr class="new-tr-${content.id}">
            <td>${content.name}</td>
            <td>${content.email}</td>
            <td>${content.password}</td>
            <td>${content.birthdate}</td>
        </tr>
        `;
};

export default showList;
