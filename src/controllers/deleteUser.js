/* requisição assíncrona para deletar uma User */
const deleteUser = async (id) => {
  try {
    const response = await fetch(`http://localhost:3000/Users/${id}`, {
      method: "DELETE",
    });
    const content = await response.json();
    return content;
  } catch (error) {
    console.log(error);
  }
};

export default deleteUser;
