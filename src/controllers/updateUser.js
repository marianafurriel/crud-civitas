import getUserById from "./getUserById.js";

const updateBtn = document.querySelector(".update");
const inputName = document.querySelector(".name");
const inputEmail = document.querySelector(".email");
const inputPass = document.querySelector(".password");
const inputBDate = document.querySelector(".birthdate");
const errSpan = document.querySelector(".error");

/* pegar o Id recebido como params */
const url = new URL(window.location);
const id = url.searchParams.get("id");

/* preencher os inputs com os atts da User */
try {
  const { name,email,password, birthdate } = await getUserById(id);
  inputName.value = name;
  inputEmail.value = email;
  inputPass.value = password;
  // const teste = bDate;
  inputBDate.value = birthdate;
} catch (error) {
  console.log(error);
}

/* atualizar User - disparar evento */
updateBtn.addEventListener("click", async (event) => {
  event.preventDefault();
  const name = inputName.value;
  const email = inputEmail.value;
  const pass = inputPass.value;
  const bDate = inputBDate.value;
  if (name === "" || email === "" || pass ===""|| bDate==="") {
    errSpan.classList.remove("invisible");
  } else {
    errSpan.classList.add("invisible");
    await updateUser(name,email,pass,bDate);
    // console.log("test");
    window.location.href = "../../public/registros.html";
  }
});

/* requisição assíncrona para atualizar a User */
const updateUser = async (name,email,pass,bDate) => {
  try {
    const response = await fetch(`http://localhost:3000/Users/${id}`, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name: `${name}`,
        email: `${email}`,
        password: `${pass}`,
        birthdate: `${bDate}`,
      }),
    });

    const content = await response.json();

    return content;
  } catch (error) {
    console.log(error);
  }
};
