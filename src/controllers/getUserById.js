/* requisição assíncrona para receber os atts da User */
const getUserById = async (id) => {
  try {
    const response = await fetch(`http://localhost:3000/Users/${id}`, {
      method: "GET",
    });
    const content = await response.json();
    console.log(content);
    return content;
  } catch (error) {
    console.log(error);
  }
};

export default getUserById;
