/* requisição assíncrona para criar uma nova User */
const postUsers = async (name, email, bDate, pass) => {
  try {
    const response = await fetch("http://localhost:3000/Users", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name: `${name}`,
        email: `${email}`,
        password: `${pass}`,
        birthdate: `${bDate}`,
      }),
    });
    const content = await response.json();

    return content;
  } catch (error) {
    console.log(error);
  }
};

export default postUsers;
