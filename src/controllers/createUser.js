import getUsers from "./getUser.js";
import postUsers from "./postUser.js";

/* get DOM elements */
const createBtn = document.querySelector("#button");
const inputEmail = document.querySelector("#email");
const inputName = document.querySelector("#name");
const inputPass = document.querySelector("#password");
const inputBDate = document.querySelector("#birthdate");
const errSpan = document.querySelector(".error");
// const table = document.querySelector(".tabela");


// /* (IIFE) - atualizar a tabela com as Users recebidas pela API */
// (async () => {
// 	await getUsers(table);
// })();

/* disparar evento para criar nova User */
createBtn.addEventListener("click", async (event) => {
	event.preventDefault();
	const name = inputName.value;
	const email = inputEmail.value;
	const bDate = inputBDate.value;
	const pass = inputPass.value;

	/* verificar se os campos estão preenchidos */
	if ((name === "") || (email === "") || (bDate === "") || (pass === "")) {
		errSpan.classList.remove("invisible");
	}
	else {
		errSpan.classList.add("invisible");
		await postUsers(name, email, bDate, pass);
	}
});
