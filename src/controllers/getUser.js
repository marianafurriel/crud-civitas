import showList from "../views/UsersView.js";
import {
  // createConcBtn,
  createUpdateBtn,
  createDeleteBtn,
} from "../utils/createButtons.js";

const table = document.querySelector(".table");



/* requisição assíncrona para receber todas as Users */
const getUsers = async (table) => {

  try {
    const response = await fetch("http://localhost:3000/Users", {
      method: "GET",
    });
    const content = await response.json();
    
    if(content.length==0){
      table.insertAdjacentHTML("beforeend",`<h2>Não há nenhum registro!</h2>`);
    }
    else{
      /* percorrer as Users recebidas */
      content.map(async (data) => {
        /* adicionar as Users na tabela */
        table.insertAdjacentHTML("beforeend", showList(data));
        /* criar os três botões de alteração de cada User */
        //createConcBtn(await data.id);
        createUpdateBtn(await data.id);
        createDeleteBtn(await data.id);
    });
    }
  } catch (error) {
    console.log(error);
  }
};

/* (IIFE) - atualizar a tabela com as Users recebidas pela API */
(async () => {
	await getUsers(table);
})();

export default getUsers;
